<?php

/**
 * @file
 * Documentation for Menu Attributes API.
 */

/**
 * Inform the menu_notification module about custom notifications.
 *
 * @return
 *   An array of notifications to be controlled by Menu Notification, keyed by
 *   notification name. Each notification record should be an array with the following
 *   key/value pairs:
 *   - #title: The human-readable name of the notification record.
 *   - label: The human-readable name of the notification property.
 *   - description: The notification description.
 *   - default: The default value.
 *
 * @see menu_notifications_get_menu_notifications_info()
 */
function hook_menu_notifications_info() {
  $info = array();

  $info['new_type'] = array(
    '#title' => t('My new notification'),
    'property1' => array(
      'label' => t('My property 1'),
      'description' => t('My description for property 1.'),
      'default' => '',
    ),
    'property2' => array(
      'label' => t('My property 2'),
      'description' => t('My description for property 2.'),
      'default' => '',
    ),
    // etc...
  );

  return $info;
}

/**
 * Alter the list of menu item notifications.
 *
 * @param $info
 *   An array of notifications to be controlled by Menu Notification, keyed by
 *   notification name.
 *
 * @see hook_menu_notifications_info()
 * @see menu_notifications_get_menu_notifications_info()
 */
function hook_menu_notifications_info_alter(array &$info) {
  // Code.
}

/**
 * Provides custom output handlers and also overriding the default handler.
 *
 * @param $title string
 *   The current title of menu item;
 *
 * @param $data array
 * contains:
 *   'settings'
 *     The notification record with the key/value pairs;
 *   'type'
 *     The machine notification name;
 *   'original_title'
 *     The original title of menu item;
 *   'tokens'
 *     Args for tokens.
 *
 * @see hook_menu_notifications_info()
 * @see menu_notifications_get_menu_notifications_info()
 */
function hook_menu_notifications_handler(&$title, $data) {
  // Code.
  if ($data['type'] == 'new_type') {
    $title .= '<span class="notification-new_type ' . $data['info']['property1'] . '">' . $data['info']['property2'] . '</span>';
  }
}
