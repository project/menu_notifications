<?php

/**
 * @file
 * Module hook implementations.
 */

/**
 * Implements hook_module_implements_alter().
 */
function menu_notifications_module_implements_alter(&$implementations, $hook) {
  if (!isset($implementations['menu_notifications'])) {
    return;
  }

  if ($hook == 'translated_menu_link_alter') {
    // Set our implementation to be the last one.
    $implementation = $implementations['menu_notifications'];
    unset($implementations['menu_notifications']);
    $implementations['menu_notifications'] = $implementation;
  }
}

/**
 * Implements hook_theme().
 */
function menu_notifications_theme() {
  $path = drupal_get_path('module', 'menu_notifications');

  $theme = array();

  $theme['menu_notifications_uses_notifications'] = array(
    'path' => $path . '/theme',
    'file' => 'theme.inc',
  );

  $theme['menu_notifications'] = array(
    'path' => $path . '/theme',
    'file' => 'theme.inc',
    'variables' => array(
      'title' => NULL,
      'classes' => NULL,
      'prefix' => NULL,
      'value' => NULL,
    ),
  );

  return $theme;
}

/**
 * Implements hook_permission().
 */
function menu_notifications_permission() {
  return array(
    'administer menu notifications' => array(
      'title' => t('Administer menu notifications'),
      'description' => t('Administer menu notifications configuration.'),
    ),
  );
}

/**
 * Implements hook_menu_notifications_info().
 */
function menu_notifications_menu_notifications_info() {
  $info = array();

  $info['default'] = array(
    '#title' => 'Default',
    'value' => array(
      'label' => t('Value'),
      'description' => t('Use "+" to calculate values. You can use any tokens here.'),
      'default' => '',
    ),
    'prefix' => array(
      'label' => t('Prefix'),
      'description' => t('Allows add a prefix to the value.'),
      'default' => '',
    ),
  );

  return $info;
}

/**
 * Fetch an array of menu notification.
 */
function menu_notifications_get_menu_notifications_info() {
  $notifications = module_invoke_all('menu_notifications_info');

  // Merge in default values.
  $info = array();
  foreach ($notifications as $notification => $settings) {
    $info[$notification]['#title'] = isset($settings['#title']) ? $settings['#title'] : '';
    foreach (array_intersect_key($settings, array_flip(element_children($settings))) as $setting => $values) {
      $values += array(
        'form' => array(),
      );
      $values['form'] += array(
        '#type' => 'textfield',
        '#title' => $values['label'],
        '#description' => isset($values['description']) ? $values['description'] : '',
        '#default_value' => isset($values['default']) ? $values['default'] : '',
      );
      $info[$notification][$setting] = $values['form'];
    }
  }

  drupal_alter('menu_notifications_info', $info);
  return $info;
}

/**
 * Implementation hook_form_FORM_ID_alter().
 */
function menu_notifications_form_menu_overview_form_alter(&$form, $form_state) {
  foreach ($form as &$item) {
    if (isset($item['mlid'], $item['#item']['options']) && isset($item['#item']['options']['notifications'])) {
      foreach ($item['#item']['options']['notifications'] as $notification) {
        if (!empty($notification['value'])) {
          $item['title']['#markup'] .= theme('menu_notifications_uses_notifications');
        }
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function menu_notifications_form_menu_edit_item_alter(&$form, $form_state) {
  $item = $form['original_item']['#value'];
  _menu_notifications_form_alter($form, $item, $form);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function menu_notifications_form_node_form_alter(&$form, $form_state) {
  if (isset($form['menu']['link']) && isset($form['#node']->menu)) {
    $item = $form['#node']->menu;
    _menu_notifications_form_alter($form['menu']['link'], $item, $form);
  }
}

/**
 * Add the menu notifications to a menu item edit form.
 */
function _menu_notifications_form_alter(array &$form, array $item = array(), array &$complete_form) {
  $form['options']['#tree'] = TRUE;
  $form['options']['#weight'] = 50;

  // Unset the previous value so that the new values get saved.
  unset($form['options']['#value']['notifications']);

  $form['options']['notifications'] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu link notifications'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );

  $notifications = menu_notifications_get_menu_notifications_info();
  foreach ($notifications as $notification => $info) {
    // Merge in the proper default value.
    if (isset($item['options']['notifications'][$notification])) {
      // If the menu link already has this notifications, use it.
      foreach ($item['options']['notifications'][$notification] as $option => $value) {
        $info['#prefix'] = '<fieldset class="fieldset-wrapper ' . $notification . '">';
        if (!empty($info['#title'])) {
          $info['#prefix'] .= '<div class="title">' . $info['#title'] . '</div>';
        }
        $info['#suffix'] = '</fieldset>';
        $info[$option]['#default_value'] = $value;
      }
    }
    $form['options']['notifications'][$notification] = $info;
  }

  // Restrict access to the new form elements.
  $user_has_access = user_access('administer menu notification');
  $form['options']['notifications']['#access'] = $user_has_access;
}

/**
 * Implements hook_translated_menu_link_alter().
 */
function menu_notifications_translated_menu_link_alter(&$item, $map) {
  // Check whether we should replace the path.
  if (!empty($GLOBALS['menu_admin']) || !isset($item['options']['notifications'])) {
    return;
  }

  $info = token_get_info();
  $data = array();

  if (module_exists('menu_token')) {
    $token_entity_mapping = token_get_entity_mapping();
    // Load data objects used when replacing link.
    if (isset($item['options']['menu_token_data'])) {
      foreach ($item['options']['menu_token_data'] as $type => $values) {
        if (!empty($info['types'][$type]) && $handler = menu_token_get_handler($values['plugin'])) {
          $values['options']['_type'] = $token_entity_mapping[$type];
          if ($object = $handler->object_load($values['options'])) {
            $data[$type] = $object;
          }
        }
      }
    }
  }

  $options['clear'] = TRUE;
  $options['sanitize'] = FALSE;
  $tokens = array('data' => $data, 'options' => $options);
  $original_title = $item['title'];
  $title = $item['link_title'];
  menu_notifications_handler($title, $item['options']['notifications']['default'], $tokens);

  // Invoke all hooks.
  foreach ($item['options']['notifications'] as $type => $settings) {
    $data = array(
      'settings' => $settings,
      'type' => $type,
      'original_title' => $original_title,
      'tokens' => $tokens,
    );

    foreach (module_implements('menu_notifications_handler') as $module) {
      $function = $module . '_menu_notifications_handler';
      $function($title, $data);
    }
  }

  $item['title'] = $title;
}

/**
 * Implements MODULE_preprocess_HOOK().
 */
function menu_notifications_preprocess_menu_link(&$variables) {
  $options = &$variables['element']['#localized_options'];
  if (isset($options['notifications'])) {
    $options['html'] = TRUE;
  }
}

/**
 * Implements MODULE_preprocess_HOOK().
 */
function menu_notifications_preprocess_links(&$variables) {
  $links = &$variables['links'];
  foreach ($links as &$link) {
    if (isset($link['notifications'])) {
      $link['html'] = TRUE;
    }
  }
}

/**
 * Provides the default notification output.
 */
function menu_notifications_handler(&$title, $info, $tokens) {
  $prefix = isset($info['prefix']) ? $info['prefix'] : '';
  $value = isset($info['value']) ? token_replace($info['value'], $tokens['data'], $tokens['options']) : '';
  if (empty($value)) {
    return;
  }

  $elements = explode('+', $value);
  $to_sum = TRUE;
  foreach ($elements as $count) {
    if (!is_numeric($count)) {
      $to_sum = FALSE;
      break;
    }
  }

  $result = 0;
  if ($to_sum) {
    foreach ($elements as $count) {
      $result += intval($count);
    }
  }
  else {
    $result = $value;
  }

  if (!empty($result)) {
    $title = theme('menu_notifications', array(
      'title' => $title,
      'classes' => 'notification-default',
      'prefix' => $prefix,
      'value' => $result,
    ));
  }
}
