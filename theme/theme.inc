<?php

/**
 * @file
 * The theme file, which controls the output of the Menu Notifications.
 */

/**
 * Appends the "(uses notifications)" label to links on the admin menu links overview form.
 */
function theme_menu_notifications_uses_notifications() {
  return ' <span class="uses-notifications">' . ' (' . t('uses notifications') . ')' . '</span>';
}

/**
 * Processes variables for menu_notifications.tpl.php.
 *
 * @see menu_notifications.tpl.php
 *
 * @ingroup theming
 *
 * Todo: Create theme.
 */
/*function template_preprocess_menu_notifications(&$variables) {

}*/
